﻿using System;
using System.Linq;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace gfoidl.Translator.Tasks
{
	public class BuildDictionary : Task
	{
		[Required]
		public string Dictionary { get; set; }

		public bool WithHeader { get; set; }

		[Required]
		public ITaskItem[] Templates { get; set; }
		//---------------------------------------------------------------------
		public BuildDictionary()
		{
			this.WithHeader = true;
		}
		//---------------------------------------------------------------------
		public override bool Execute()
		{
#if DEBUG
			this.Log.LogMessage("# Templates = {0}", this.Templates.Length);
#endif
			if (this.Templates.Length == 0) return !this.Log.HasLoggedErrors;

			try
			{
				var tokenExtractor = new TokenExtractor();
				var dic 		   = new Dictionary(this.Dictionary, this.WithHeader);
				var tokens 		   = tokenExtractor.GetOrderedTokens(this.Templates.Select(it => it.ItemSpec));
				dic.WriteTokensToCsv(tokens);
			}
			catch (Exception ex)
			{
				this.Log.LogErrorFromException(ex);
			}

			return !this.Log.HasLoggedErrors;
		}
	}
}