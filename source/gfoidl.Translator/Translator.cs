﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace gfoidl.Translator
{
	public class Translator : TokenBase
	{
		private readonly Dictionary<string, string> _dic;
		//---------------------------------------------------------------------
		public Translator(Dictionary<string, string> dictionary)
		{
			if (dictionary == null)
				throw new ArgumentNullException("dictionary");
			Contract.EndContractBlock();

			_dic = dictionary;
		}
		//---------------------------------------------------------------------
		public void Translate(IEnumerable<string> files)
		{
			IList<string> filesList = files as IList<string>;
			if (filesList != null && filesList.Count == 1)
			{
				this.Translate(filesList[0]);
				return;
			}

			Parallel.ForEach(files, this.Translate);
		}
		//---------------------------------------------------------------------
		public void Translate(string file)
		{
			if (file == null)
				throw new ArgumentNullException("file");

			if (!File.Exists(file))
				throw new FileNotFoundException("File does not exist", file);

			Contract.EndContractBlock();

			string content 	   = File.ReadAllText(file);
			string translation = this.TranslateInternal(content);
			File.WriteAllText(file, translation);
		}
		//---------------------------------------------------------------------
		internal string TranslateInternal(string content)
		{
			string translation = Regex.Replace(content, _pattern, match =>
			{
				string replacement;
				if (_dic.TryGetValue(match.Groups[1].Value, out replacement))
					return replacement;

				return match.Value;
			});

			return translation;
		}
	}
}