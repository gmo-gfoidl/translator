﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;

namespace gfoidl.Translator
{
	public class Dictionary
	{
		private readonly string _fileName;
		private readonly bool 	_hasHeader;
		//---------------------------------------------------------------------
		public string FileName { get { return _fileName; } }
		public bool HasHeader  { get { return _hasHeader; } }
		//---------------------------------------------------------------------
		public Dictionary(string fileName, bool hasHeader = true)
		{
			if (fileName == null)
				throw new ArgumentNullException("fileName");
			Contract.EndContractBlock();

			if (!Path.HasExtension(fileName))
				fileName = Path.Combine(fileName, "dictionary.csv");

			_fileName  = fileName;
			_hasHeader = hasHeader;
		}
		//---------------------------------------------------------------------
		public void WriteTokensToCsv(IEnumerable<string> tokens, bool appendDummyTranslation = true, char separator = ';')
		{
			if (tokens == null)
				throw new ArgumentNullException("tokens");
			Contract.EndContractBlock();

			Directory.CreateDirectory(Path.GetDirectoryName(_fileName));
			using (StreamWriter sw = File.CreateText(_fileName))
			{
				if (_hasHeader)
					sw.WriteLine("token{0}translation", separator);

				foreach (string token in tokens)
					if (appendDummyTranslation)
						sw.WriteLine("{0}{1}{0}", token, separator);
					else
						sw.WriteLine("{0}{1}", token, separator);
			}
		}
		//---------------------------------------------------------------------
		public Dictionary<string, string> ReadTokensFromCsv(int column = 1, char separator = ';')
		{
			if (column < 1)
				throw new ArgumentOutOfRangeException("column", "Column must be greater or equal to 1. Column 0 is the token, so starting with 1 the translations are listed.");
			Contract.EndContractBlock();

			Dictionary<string, string> dic = new Dictionary<string, string>();

			using (StreamReader sr = File.OpenText(_fileName))
			{
				string[] cols = sr.ReadLine().Split(separator);

				if (column >= cols.Length)
					throw new ArgumentException(
						string.Format("Column (zero-based) {0} does not exist in file '{1}'. Check if separator '{2}' is correct.", column, _fileName, separator),
						"column");


				if (!_hasHeader)
					dic[cols[0]] = cols[column];

				while (!sr.EndOfStream)
				{
					cols 		 = sr.ReadLine().Split(separator);
					dic[cols[0]] = cols[column];
				}
			}

			return dic;
		}
	}
}