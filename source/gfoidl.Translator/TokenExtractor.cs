﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace gfoidl.Translator
{
	public class TokenExtractor : TokenBase
	{
		public IEnumerable<string> GetOrderedTokens(IEnumerable<string> templateFiles)
		{
			if (templateFiles == null)
				throw new ArgumentNullException("templateFiles");

			foreach (string file in templateFiles)
				if (!File.Exists(file)) throw new FileNotFoundException("File not found", file);
			//Contract.EndContractBlock();		--> geht nicht, wegen foreach

			var tokens = templateFiles.AsParallel()
				.SelectMany(tf => this.ProcessTemplate(File.ReadAllText(tf)))
				.GroupBy(t => t)
				.Select(grp => grp.Key)
				.OrderBy(t => t);

			return tokens;
		}
		//---------------------------------------------------------------------
		public IEnumerable<string> ProcessTemplate(string template)
		{
			if (template == null)
				throw new ArgumentNullException("template");
			Contract.EndContractBlock();

			MatchCollection matches = Regex.Matches(template, _pattern);

			foreach (Match match in matches)
				yield return match.Groups[1].Value;		// Groups[0] wäre der Match selbst
		}
	}
}