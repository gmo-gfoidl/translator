﻿using System;
using System.IO;

namespace gfoidl.Translator
{
	internal class HelpRunner : Runner
	{
		public override void Run()
		{
			var version = typeof(HelpRunner).Assembly.GetName().Version;
			Console.WriteLine("Translator (c) 2016 Günther Foidl -- ver {0}.{1}.{2}\n\n", version.Major, version.Minor, version.Revision);

			using (Stream stream   = typeof(HelpRunner).Assembly.GetManifestResourceStream(typeof(HelpRunner), "Helptext.txt"))
			using (StreamReader sr = new StreamReader(stream))
			{
				while (!sr.EndOfStream)
				{
					string line = sr.ReadLine();

					line.ConsoleWriteFormatted();
				}
			}
		}
		//---------------------------------------------------------------------
		protected override void RunCore()
		{
			throw new NotImplementedException();
		}
	}
}