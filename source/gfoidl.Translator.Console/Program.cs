﻿using System;
using System.Diagnostics;
using System.Linq;

namespace gfoidl.Translator
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				Runner runner = GetRunner(args);

				runner.Run();
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}

			Console.WriteLine("\nbye");
			if (Debugger.IsAttached)
				Console.ReadKey();
		}
		//---------------------------------------------------------------------
		private static Runner GetRunner(string[] args)
		{
			if (args == null || args.Length < 2)
				return new HelpRunner();

			string mode = args[0];

			switch (mode)
			{
				case "translate": return new TranslationRunner(args.Skip(1).ToArray());
				case "extract": return new ExtractionRunner(args.Skip(1).ToArray());
				default:
					return new HelpRunner();
			}
		}
	}
}