﻿using System.Collections.Generic;
using System.Linq;

namespace gfoidl.Translator
{
	internal class ExtractionRunner : Runner
	{
		public ExtractionRunner(string[] args) : base(args, 2) { }
		//---------------------------------------------------------------------
		protected override void RunCore()
		{
			this.LogMessage("in extraction mode.");

			this.LogMessage("gathering files...");
			List<string> templateFiles = this.GatherFiles().ToList();
			this.LogMessage("\t{0} files gathered.", templateFiles.Count);

			this.LogMessage("extracting tokens...");
			TokenExtractor tokenExtractor = new TokenExtractor();
			List<string> tokens 		  = tokenExtractor.GetOrderedTokens(templateFiles).ToList();
			this.LogMessage("\t{0} tokens extracted.", tokens.Count);

			this.LogMessage("writing dictionary...");
			Dictionary dictionary = new Dictionary(_args[1]);
			dictionary.WriteTokensToCsv(tokens);

			string message = string.Format("\tdictionary written to\n{0}", dictionary.FileName);
			("#dg{" + message + "}").ConsoleWriteFormatted();
		}
	}
}