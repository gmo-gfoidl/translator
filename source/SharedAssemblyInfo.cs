﻿using System.Reflection;

[assembly: AssemblyCompany("Foidl Günther")]
[assembly: AssemblyProduct("gfoidl.Translator")]
[assembly: AssemblyCopyright("Copyright © Foidl Günther 2016")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]