﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace gfoidl.Translator.Tests
{
	namespace TokenExtractorTest
	{
		[TestFixture]
		public class ProcessTemplate
		{
			[Test]
			[ExpectedException(typeof(ArgumentNullException))]
			public void Template_is_null___Throws_ANE()
			{
				TokenExtractor sut = new TokenExtractor();

				var actual = sut.ProcessTemplate(null).ToList();

				Assert.AreEqual(1, 1);
			}
			//---------------------------------------------------------------------
			[Test]
			public void Template_is_empty___Returns_empty_collection([Values("", "\t", "\n")]string template)
			{
				TokenExtractor sut = new TokenExtractor();

				var actual = sut.ProcessTemplate(template).ToList();

				Assert.AreEqual(0, actual.Count);
			}
			//---------------------------------------------------------------------
			[Test]
			public void Template_is_given___tokens_returned_by_occurance()
			{
				string template = @"
# ${Header} 1

${Name}: Günther  
${Age}: 33  
${Sex}: ${male}  

${Name}: Bella  
${Age}: 21  
${Sex}: ${female}";

				TokenExtractor sut = new TokenExtractor();

				var actual = sut.ProcessTemplate(template).ToArray();

				string[] expected = { "Header", "Name", "Age", "Sex", "male", "Name", "Age", "Sex", "female" };

				CollectionAssert.AreEqual(expected, actual);
			}
			//---------------------------------------------------------------------
			[Test]
			public void Template_given_with_nested_curly_brackets___tokens_returned()
			{
				string template = @"
# ${Header} 1

${weil es lustig {...} ist}";

				TokenExtractor sut = new TokenExtractor();

				var actual = sut.ProcessTemplate(template).ToArray();

				string[] expected = { "Header", "weil es lustig {...} ist" };

				CollectionAssert.AreEqual(expected, actual);
			}
			//---------------------------------------------------------------------
			[Test]
			public void Template_given_with_nested_unbalanced_curly_brackets___unbalanced_is_not_recognized_as_token()
			{
				string template = @"
# ${Header} 1

${weil es lustig {... ist}";

				TokenExtractor sut = new TokenExtractor();

				var actual = sut.ProcessTemplate(template).ToArray();

				string[] expected = { "Header" };

				CollectionAssert.AreEqual(expected, actual);
			}
			//---------------------------------------------------------------------
			[Test]
			public void Template_given_with_deep_nested_curly_brackets___tokens_returned()
			{
				string template = @"
# ${Header} 1

${weil es lustig {.{x}..} ist}";

				TokenExtractor sut = new TokenExtractor();

				var actual = sut.ProcessTemplate(template).ToArray();

				string[] expected = { "Header", "weil es lustig {.{x}..} ist" };

				CollectionAssert.AreEqual(expected, actual);
			}
		}
		//---------------------------------------------------------------------
		[TestFixture]
		public class GetOrderedTokens
		{
			[Test]
			[ExpectedException(typeof(ArgumentNullException))]
			public void TokenFiles_is_null___Throws_ANE()
			{
				TokenExtractor sut = new TokenExtractor();

				sut.GetOrderedTokens(null);
			}
			//---------------------------------------------------------------------
			[Test]
			[ExpectedException(typeof(FileNotFoundException))]
			public void TokenFile_does_not_exist___Throws_FileNotFound()
			{
				TokenExtractor sut = new TokenExtractor();

				var actual = sut.GetOrderedTokens(new string[] { "data/foo.txt" }).ToList();
			}
			//---------------------------------------------------------------------
			[Test]
			public void Files_given___Tokens_unique_and_ordered_returned()
			{
				string[] files = { "data/hallo.html", "data/text.md" };

				TokenExtractor sut = new TokenExtractor();

				var actual = sut.GetOrderedTokens(files).ToArray();

				string[] expected = { "es ist schön", "Header", "Name", "Age", "Sex", "male", "female" };
				Array.Sort(expected);

				CollectionAssert.AreEqual(expected, actual);
			}
		}
	}
}