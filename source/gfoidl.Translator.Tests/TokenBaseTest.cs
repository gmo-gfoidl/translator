﻿using System;
using NUnit.Framework;

namespace gfoidl.Translator.Tests
{
	namespace TokenBaseTest
	{
		[TestFixture]
		public class Pattern
		{
			[Test]
			[ExpectedException(typeof(ArgumentNullException))]
			public void Pattern_is_null_or_empty___Throws_ANE([Values(null, "", " ", "\t")] string pattern)
			{
				TokenExtractor sut = new TokenExtractor();

				sut.Pattern = pattern;

				Assert.AreEqual(pattern, sut.Pattern);
			}
			//---------------------------------------------------------------------
			[Test]
			public void Pattern_is_not_given___default_pattern_is_used()
			{
				TokenExtractor sut = new TokenExtractor();

				string expected = @"\${((?:[^{}]|(?<counter>{)|(?<-counter>}))*)(?(counter)(?!))}";
				string actual = sut.Pattern;

				Assert.AreEqual(expected, actual);
			}
			//---------------------------------------------------------------------
			[Test]
			public void Pattern_is_given___pattern_is_used()
			{
				string expected = @"\@|[^|]*|\@";

				TokenExtractor sut = new TokenExtractor();
				sut.Pattern 	   = expected;

				Assert.AreEqual(expected, sut.Pattern);
			}
		}
	}
}