# Unit Tests

## Organisation

Ist mir selbst auch eingefallen, aber in [Zen Coding: Structuring Unit Tests](http://zendeveloper.blogspot.co.at/2012/01/structuring-unit-tests.html) ist es schon passend beschrieben.

## Benennung

Das Schema von [Naming standards for unit tests - Osherove](http://osherove.com/blog/2005/4/3/naming-standards-for-unit-tests.html) fand und finde ich ganz passend, nur mit einer Modifikation die obige Orginasation berücksichtigt und mMn leserlicher ist.

`State_under_Test___expected_Behavior` 

Es entspricht also diesem Regex-Pattern: `^([^_]+_)*[^_]+_{3}([^_]+_)*[^_]+$`

(c) 2015 gfoidl