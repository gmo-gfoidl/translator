[![Build status](https://ci.appveyor.com/api/projects/status/onud4srifalxaxkr/branch/master?svg=true)](https://ci.appveyor.com/project/GntherFoidl/translator/branch/master)

# Contents

[TOC]

---
# Translator (Console)

## General

A tool for template-based translation with dictionaries.    

Translator has two modes:  

* _translate_  
* _extract_

## Basic Workflow

1. create template(s) -- this can be any text-based file  

2. surround the strings, that should be replaced -- called tokens, with `${ }`.  
   For instance `${my} ${Name}` or `${xyz}` or whole sentences.    

3. build a dictionary by running `Translator extract path-to-template(s) path-to-dictionary`  

4. a generic dicionary.csv is generated, that contains the tokens  
   (alphabetically ordered) in the format `token;translation`  

5. localize the content of this dictionary, so that in the translation-column
   the translated word for the takens are placed  
   There can be as many translation-columns as you want  
   (typically there's only one)  
   For instance:  
   `token;en;de;it`  
   or only:  
   `token;en`  

6. copy the templates to the desired path for the next step

7. translate the template(s) by running  
   `Translator translate path-to-copied-templates path-to-dictionary column-index-of-translation-in-dictionary`  

## Arguments

`path-to-template(s)` can be either a single file, a column separated (;)
set of files, or a folder containing the files.  
In case of a folder it is scanned recursiveley for (text-) files.  
For `path-to-copied-templates` the same rules apply.

If `path-to-dictionary` points to a folder the filename #dg{dictionary.csv} will be used.

`column-index` is zero-based, i.e. the token=0;en=1;de=2;it=3 from the above example

Switch `-v` is for verbosity.

---
# MsBuild-Target

## General

There is also a _MsBuild-Target_, that has basically the same function as the _Translator_ described above.  
The task can be installed via NuGet [gfoidl.Translator.Tasks](https://www.nuget.org/packages/gfoidl.Translator.Tasks/) and the Build-Target gets injected in the project-file (by NuGet).

Before the actual build the template-files are copied to the destination (see below), and translated by selecting the appriate dictionary following the convention of matching culture-names.

The translation is done every culture specified in the settings.

## Default Settings

By default, templates are expected to reside in `template`-folder, and all files in this folder are recursively included in the process of either extracting the tokens or translating based on the dictionary.  
For translation the files are copied to `data/{culture}`, where {culture} is replaced by the culture-identifier as specified by the user (see later). Default cultures are _de_, and _en_. Dictionary files are expected to reside in `strings`-folder with the name following the convention `{culture}.csv` -- so for instance `strings/de.csv`

## Options

Only basis knowledge of MsBuild is necessary to set custom options.  
To set custom options you can edit the _gfoidl.Translator.Tasks.targets_ file directly, but this is discouraged because every NuGet-update will override this file. Therefore it is adviced to modify the csproj-file (at the end, after _gfoidl.Translator.Tasks.targets_ is imported, so that the custom-value override the default value -- this is a behavior of MsBuild).

### Cultures

In this example the german (de) culture is removed from the project, and the italian (it) culture is added.  
Notice: the identifier of the culture is up to you. It is only important that the name of the dictionary corresponds to the culture.
```
#!xml
<ItemGroup Label="Cultures">
	<Culture Remove="de" />
	<Culture Include="en" />
	<Culture Include="it" />
</ItemGroup>
```

### Properties that can be set
```
#!xml
<PropertyGroup>
	<TemplateDir>template</TemplateDir>
	<TemplateExtension>*</TemplateExtension>
	<DataDir>data</DataDir>
	<StringsDir>strings</StringsDir>
</PropertyGroup>
```

### Hooking-in the target

```
#!xml
<Target Name="Translate" BeforeTargets="BeforeBuild">
	<!-- -->
</Target>
```

The snippet above shows the "entry point" of _gfoidl.Tra.Tasks.targets_. You can hook in with your own target in respect to this target.